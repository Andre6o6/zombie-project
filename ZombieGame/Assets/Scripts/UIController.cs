﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;


public class UIController : MonoBehaviour
{
    public Slider healthBar;
    public Image healthImage;

    // Use this for initialization
    void Start ()
    {
		healthBar.maxValue = GameManager.instance.player.maxHealth;
    }

    // Update is called once per frame
    void Update ()
    {
		healthBar.value = GameManager.instance.player.health;
	}
}
