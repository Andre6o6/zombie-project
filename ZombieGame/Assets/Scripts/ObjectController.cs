﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour {

	public int size;
	public float health;
	//public Sprite[] sprites; 
	public enum Pos	{ All, Corner, Wall, Middle };
	public Pos prefPosition;
	public bool passable;
	public bool piercable;
	public bool destructible;
	public bool container;
	public GameObject[] spawnObjects;

}
