﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
 
//not used 'cause isn't serializable
public class Pair<T1, T2>
{
	public T1 a;
	public T2 b;

	public Pair ()
	{
		this.a = default(T1);
		this.b = default(T2);
	}
	public Pair (T1 a, T2 b)
	{
		this.a = a;
		this.b = b;
	}
}

[System.Serializable]
public class PairRI
{
	public Room a;
	public int b;

	public PairRI ()
	{
		this.a = null;
		this.b = 0;
	}
	public PairRI (Room a, int b)
	{
		this.a = a;
		this.b = b;
	}
}
[System.Serializable]
public class PairGF
{
	public GameObject a;
	public float b;

	public PairGF ()
	{
		this.a = null;
		this.b = 0f;
	}
	public PairGF (GameObject a, float b)
	{
		this.a = a;
		this.b = b;
	}
}

[System.Serializable]
public class Area
{
	public Vector2 v1;
	public Vector2 v2;
	public Area ()
	{
		this.v1 = Vector2.zero;
		this.v2 = Vector2.zero;
	}
	public Area (Vector2 v1, Vector2 v2)
	{
		this.v1 = v1;
		this.v2 = v2;
	}
}
[System.Serializable]
public class Room
{
	public string type;
	public int minA;
	public int minB;
	public int maxA;
	public int maxB;
	public List<PairGF>objects;		//масс возможных объектов и вероятностей

	public Room ()
	{
		this.objects = new List<PairGF> ();
	}
	public Room (string type, int minA, int minB, int maxA, int maxB)
	{
		this.type = type;
		this.maxA = maxA;
		this.maxB = maxB;
		this.minA = minA;
		this.minB = minB;
		this.objects = new List<PairGF> ();
	}
}
[System.Serializable]
public class Building
{
	public string type;

	public List<PairRI> rooms;
	public int minSqr;
	public int minDim;
	public Building()
	{
		this.rooms = new List<PairRI>();
	}
	public Building (string type, int minSqr, int minDim)
	{
		this.type = type;
		this.rooms = new List<PairRI>();
		this.minSqr = minSqr;
		this.minDim = minDim;
	}
}

[System.Serializable]
public class MapGenerator : MonoBehaviour {

	private static MapGenerator _instance;
	public static MapGenerator instance
	{
		get
		{ 
			if (_instance == null) {
				_instance = (MapGenerator) FindObjectOfType(typeof(MapGenerator));
			
				if (_instance == null)
				{
					GameObject singleton = new GameObject();
					_instance = singleton.AddComponent<MapGenerator>();
					singleton.name = "Map";

					DontDestroyOnLoad(singleton);

					Debug.Log("[MapGenerator] An instance of " + singleton + "' was created with DontDestroyOnLoad.");
				} 
			}
			return _instance;
		}
	}

	public int seed;
	public int height;
	public int width;
	public int roadCount;
	public int minRoadWidth;
	public int maxRoadWidth;
	public int minDelta;
	public float popCoef;	//регулирует плотность застройки
	public GameObject[] tiles;
	public GameObject[] enemies;
	[HideInInspector] public int[,] mapInt;

	const int EMPTY = 0;
	const int ROAD = 1;
	const int BUILDING_AREA = 2;
	const int TREE = 3;
	const int DOOR = 4;
	//...
	const int WALL = 5;

	public List<Room> possibleRooms;
	public List<Building> possibleBuildings; 

	void Awake () {
		
		DontDestroyOnLoad(instance.gameObject);
	}

	void Start() {
		GenerateTown (seed);
	}
		
	void GenerateWorld(int _seed) {
		//генерация глобальной карты мира
	}
	void GenerateTown(int _seed)
	{
		mapInt = new int[height, width];
		Random.InitState(_seed);
		/*
		 * План
		 *1) дороги
		 *2) стены
		 *3) двери, ключи->mustSpawnObjects
		 *4) объекты, расстановка, дичь типа стульев рядом со сталами (? объект spawnZone)
		 *5) наполнение контейнеров, спавн мелких pickable объектов, mustSpawn объектов
		 *6) деревья и машины
		 *7) враги и нпс
		 */


		//Генерация дорог
		Queue<Area> areas = GenerateRoads();
		//Генерация зданий
		GenerateBuildings(areas);

		//Генерация деревьев, машин и т.д.
		//массив экстернальных обьектов, рандомная т-ка на карте
		GenerateOther();
		GenerateEnemies ();	//в домах?

		//Инстанциирование всего дерьма
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++) {
				if (mapInt [i, j] == WALL) {	
					var wall = Instantiate (tiles [WALL], new Vector3 (j, i, -1f), transform.rotation);
					//поворот стены и смена спрайта
					//1 - низ, 2 - лево, 3 - верх, 4 - право
					int s = 0;
					Vector3 rot = new Vector3 ();
					if (i > 0 && (mapInt [i - 1, j] == WALL || mapInt [i - 1, j] == DOOR)) 
						s = 1;
					if (j > 0 && (mapInt [i, j - 1] == WALL || mapInt [i, j - 1] == DOOR)) 
						s = 10 * s + 2;
					if (i < height - 1 && (mapInt [i + 1, j] == WALL || mapInt [i + 1, j] == DOOR)) 
						s = 10 * s + 3;
					if (j < width - 1 && (mapInt [i, j + 1] == WALL || mapInt [i, j + 1] == DOOR))
						s = 10 * s + 4;

					if (s == 1234) {
						s = 3;
						rot.z = 0;
					} else if (s > 100) {
						if (s == 124)
							rot.z = 0;
						else if (s == 123)
							rot.z = -90;
						else if (s == 234)
							rot.z = 180;
						else if (s == 134)
							rot.z = 90;
						s = 2;
					} else if (s == 13 || s == 24) {
						wall.GetComponent<BoxCollider2D> ().size = new Vector2(0.56f, 1f);
						if (s == 24)
							rot.z = 90;
						s = 0;
					} else {
						if (s == 12)
							rot.z = -90;
						else if (s == 23)
							rot.z = 180;
						else if (s == 34)
							rot.z = 90;
						s = 1;
					}
					Quaternion q = new Quaternion ();
					q.eulerAngles = rot;
					wall.transform.rotation = q;
					wall.GetComponent<SpriteRenderer> ().sprite = wall.GetComponent<Wall> ().sprites [s];

				} else if (mapInt [i, j] == DOOR) {
					Quaternion rot = transform.rotation;
					float z = 0;
					if (mapInt [i + 1, j] == WALL || mapInt [i + 1, j] == DOOR)
						z = 90;
					if (mapInt [i, j + 1] == DOOR)
						z += 180;
					rot.eulerAngles = new Vector3(0, 0, z);

					//TODO баррикады вместо заблокированных дверей
					var door = (GameObject)Instantiate (tiles [DOOR], new Vector3 (j, i, 0f), rot);
				} else //if (mapInt [i, j] != EMPTY) 	//?
					Instantiate (tiles [mapInt [i, j]], new Vector3 (j, i, 1f), transform.rotation);
			}
		}
	}
	Queue<Area> GenerateRoads()	{
		Queue<Area> areas = new Queue<Area>();
		areas.Enqueue(new Area(new Vector2(0f, 0f), new Vector2(width-1, height-1)));

		int x; int y;
		for (int i = 0; i < roadCount; i++) {
			//Оптимизировать?
			Area curArea = areas.Dequeue();

			if (Mathf.Abs(curArea.v2.x - curArea.v1.x) < minDelta || Mathf.Abs(curArea.v2.y - curArea.v1.y) < minDelta) {
				areas.Enqueue (curArea);
				continue;
			}

			Area newArea1 = new Area ();
			Area newArea2 = new Area ();

			float rndNmr = Random.Range ( 0f, (curArea.v2.x-curArea.v1.x)/(curArea.v2.y-curArea.v1.y) ) - Random.Range(0f, 1f);
			int roadWidth;
			int dir = rndNmr > 0 ? 1 : 0;
			switch (dir) {
			case 0:		//road || Ox
				roadWidth = Random.Range (minRoadWidth, maxRoadWidth + 1);
				if ((curArea.v2.x - curArea.v1.x) < 3 * roadWidth)
					roadWidth /= 2;
				if (roadWidth > Mathf.Abs (curArea.v2.y - curArea.v1.y) - 1)
					roadWidth = (int)Mathf.Abs (curArea.v2.y - curArea.v1.y) - 1;
				if (roadWidth < 0)
					roadWidth = 0;

				y = Random.Range ((int)curArea.v1.y + 1, (int)curArea.v2.y - roadWidth);
				newArea1.v1 = new Vector2 (curArea.v1.x, curArea.v1.y);
				newArea1.v2 = new Vector2 (curArea.v2.x, y - 1);
				newArea2.v1 = new Vector2 (curArea.v1.x, y + roadWidth);
				newArea2.v2 = new Vector2 (curArea.v2.x, curArea.v2.y);

				for (int j = (int)curArea.v1.x; j <= curArea.v2.x; j++)
					for (int k = 0; k < roadWidth; k++) 
					{
						mapInt [y + k, j] = ROAD;
					}
				break;
			case 1:		//road || Oy
				roadWidth = Random.Range (minRoadWidth, maxRoadWidth + 1);
				if ((curArea.v2.y - curArea.v1.y) < 3 * roadWidth)
					roadWidth /= 2;
				if (roadWidth > Mathf.Abs (curArea.v2.x - curArea.v1.x) - 1) 
					roadWidth = (int)Mathf.Abs (curArea.v2.x - curArea.v1.x) - 1;
				if (roadWidth < 0)
					roadWidth = 0;

				x = Random.Range ((int)curArea.v1.x + 1, (int)curArea.v2.x - roadWidth);
				newArea1.v1 = new Vector2 (curArea.v1.x, curArea.v1.y);
				newArea1.v2 = new Vector2 (x - 1, curArea.v2.y);
				newArea2.v1 = new Vector2 (x + roadWidth, curArea.v1.y);
				newArea2.v2 = new Vector2 (curArea.v2.x, curArea.v2.y);

				for (int j = (int)curArea.v1.y; j <= curArea.v2.y; j++) 
					for (int k = 0; k < roadWidth; k++) 
					{
						mapInt [j, x + k] = ROAD;
					}
				break;
			}

			areas.Enqueue (newArea1);
			areas.Enqueue (newArea2);
		}

		return areas;
	}
	void GenerateBuildings(Queue<Area> areas){
		float rndNmr;
		int dir;
		int sqr;
		int a;
		int b;
		int[,] probMask;	//маска коэф. вероятностей; используется при генерировании объектов в комнате
		Queue<Area> halls = new Queue<Area> ();
		while (areas.Count > 0) {
			Area curArea = areas.Dequeue();
			if (curArea.v2.x - curArea.v1.x + 1 < 4 || curArea.v2.y - curArea.v1.y + 1 < 4)
				continue;

			curArea.v1 += new Vector2 (1, 1);
			curArea.v2 -= new Vector2 (1, 1);

			a = (int)(curArea.v2.x - curArea.v1.x + 1);
			b = (int)(curArea.v2.y - curArea.v1.y + 1);
			sqr = a * b;

			//выбрать тип здания
			List<Building> posBuilding = new List<Building>();
			for (int i = 0; i < possibleBuildings.Count; i++)
				if (sqr >= possibleBuildings [i].minSqr && a >= possibleBuildings [i].minDim && b >= possibleBuildings [i].minDim)
					//TODO : условия на кол-во на районе
					posBuilding.Add (possibleBuildings [i]);
			if (posBuilding.Count == 0)
				continue;
			Building curBuilding = posBuilding [Random.Range (0, posBuilding.Count)];

			//поделить если измерения большие
			if (sqr > curBuilding.minSqr * popCoef) {	// можно размер здания регулировать этим коэф (больше коэф -> более плотно и большие здания)
				// а в коллекциях определять комнаты
				rndNmr = Random.Range (0f, (curArea.v2.x - curArea.v1.x) / (curArea.v2.y - curArea.v1.y)) - Random.Range (0f, 1f);
				dir = rndNmr > 0 ? 1 : 0;
				if (b > 2 * a)
					dir = 0;
				if (a > 2 * b)
					dir = 1;
				Area newArea = new Area ();
				if (dir == 0) {
					//a - fix
					//b = curBuilding.minSqr / a + Random.Range(1, (b - curBuilding.minSqr / a) / 4);
					b = b/2 + + Random.Range(-1,2) * Random.Range(1, b/6);  //стена делится пополам +- 1/6
					int y = (int)curArea.v1.y + b;
					newArea.v1 = new Vector2 (curArea.v1.x, y + Random.Range(1, 4/*(int)(curArea.v2.y - curArea.v1.y + 1 - b) / 4*/)); //добавляется пустая полоса 
					newArea.v2 = new Vector2(curArea.v2.x, curArea.v2.y);
					curArea.v2 = new Vector2 (curArea.v2.x, y);
				}
				if (dir == 1) {
					//b - fix
					//a = curBuilding.minSqr / b + Random.Range(1, (a - curBuilding.minSqr / b) / 4);
					a = a/2 + Random.Range(-1,2) * Random.Range(1, a/6);
					int x = (int)curArea.v1.x + a;
					newArea.v1 = new Vector2(x + Random.Range(1, 4/*(int)(curArea.v2.x - curArea.v1.x + 1 - a) / 4*/), curArea.v1.y);
					newArea.v2 = new Vector2(curArea.v2.x, curArea.v2.y);
					curArea.v2 = new Vector2 (x, curArea.v2.y);
				}
				areas.Enqueue (newArea);
				areas.Enqueue (curArea);
				continue;
			}

			//начальные пол и стены
			int doorCount = Random.Range(1,5);
			for (int i = (int)curArea.v1.x; i <= curArea.v2.x; i++) {
				//усл. для двери - не угол, остались свободные двери и рандом
				if (i > curArea.v1.x && i < curArea.v2.x && doorCount > 0 && Random.Range (0, (int)(curArea.v2.x - curArea.v1.x)) < doorCount) {	
					doorCount--;
					mapInt [(int)curArea.v1.y, i] = DOOR;
				}
				else
					mapInt[(int)curArea.v1.y, i] = WALL;
				if (i > curArea.v1.x && i < curArea.v2.x && doorCount > 0 && Random.Range (0, (int)(curArea.v2.x - curArea.v1.x)) < doorCount) {
					doorCount--;
					mapInt [(int)curArea.v2.y, i] = DOOR;
				}
				else
					mapInt[(int)curArea.v2.y, i] = WALL;
			}
			for (int i = (int)curArea.v1.y; i <= curArea.v2.y; i++) {
				if (i > curArea.v1.y && i < curArea.v2.y && doorCount > 0 && Random.Range (0, (int)(curArea.v2.y - curArea.v1.y)) < doorCount) {
					doorCount--;
					mapInt [i, (int)curArea.v1.x] = DOOR;
				}
				else 
					mapInt [i, (int)curArea.v1.x] = WALL;
				if ( i > curArea.v1.y && i < curArea.v2.y && doorCount > 0 && Random.Range (0, (int)(curArea.v2.y - curArea.v1.y)) < doorCount) {
					doorCount--;
					mapInt[i, (int)curArea.v2.x ] = DOOR;
				}
				else
					mapInt[i, (int)curArea.v2.x ] = WALL;
			}
			if (doorCount > 0)
				mapInt [(int)curArea.v1.y, Random.Range((int)curArea.v1.x + 1, (int)curArea.v2.x)] = DOOR;

			for (int i = (int)curArea.v1.x + 1; i < curArea.v2.x; i++)
				for (int j = (int)curArea.v1.y + 1; j < curArea.v2.y; j++)
					mapInt [j, i] = BUILDING_AREA;

			curArea.v1 += new Vector2 (1, 1);
			curArea.v2 -= new Vector2 (1, 1);

			//рекурсивно делить на комнаты, смотря на те комнаты, которые нужны
			halls.Enqueue (curArea);
			while (halls.Count > 0) {
				Area curHall = halls.Dequeue ();

				a = (int)(curHall.v2.x - curHall.v1.x + 1);
				b = (int)(curHall.v2.y - curHall.v1.y + 1);
				sqr = a * b;
				if (sqr < 5)
					continue;

				//TODO : нормальный выбор комнаты
				Room curRoom = curBuilding.rooms[ Random.Range(0, curBuilding.rooms.Count) ].a;

				Area newHall1 = new Area ();
				Area newHall2 = new Area ();

				if (a <= curRoom.minA)
					dir = 0;
				else if (b <= curRoom.minA)
					dir = 1;
				else {
					rndNmr = Random.Range (0f, (curHall.v2.x - curHall.v1.x) / (curHall.v2.y - curHall.v1.y)) - Random.Range (0f, 1f);
					dir = rndNmr > 0 ? 1 : 0;
				}
				int delta;
				int w;
				float decX = 0; float decY = 0;
				switch (dir) {
				case 0:	// wall || Ox
					int y = 0;
					if (b > 2) {	//если есть место для стены (т.е. не получится 0-dim комнаты)
						delta = (int)(Mathf.Sign (a - curRoom.minB) * Random.Range (0, Mathf.Abs (a - curRoom.minB) + 1));

						w = Random.Range (curRoom.minB - delta, curRoom.maxA - delta);
						if (w < curHall.v1.y + 1 || w > b - 2)
							w = Random.Range (curRoom.minB, curRoom.maxA);
						if (w > b - 2)
							w = b / 2;
						y = (int)(curHall.v1.y + w);
						//двигаем дверь
						if (mapInt [y, (int)curHall.v2.x + 1] != WALL) {
							mapInt [y, (int)curHall.v2.x + 1] = WALL;
							mapInt [y + 1, (int)curHall.v2.x + 1] = DOOR;
						}
						if (mapInt [y, (int)curHall.v1.x - 1] != WALL) {
							mapInt [y, (int)curHall.v1.x - 1] = WALL;
							mapInt [y - 1, (int)curHall.v1.x - 1] = DOOR;
						}


						newHall1.v1 = new Vector2 (curHall.v1.x, curHall.v1.y);
						newHall1.v2 = new Vector2 (curHall.v2.x, y - 1);
						newHall2.v1 = new Vector2 (curHall.v1.x, y + 1);
						newHall2.v2 = new Vector2 (curHall.v2.x, curHall.v2.y);

						int doorX = Random.Range ((int)curHall.v1.x, (int)curHall.v2.x);
						for (int j = (int)curHall.v1.x; j <= curHall.v2.x; j++)
							if (j != doorX) {
								mapInt [y, j] = WALL;
							} else {
								mapInt [y, j] = DOOR;
							}
					} else {	//если не можем поставить стену, работаем только с одним холлом
						y = (int)curHall.v1.y + b;
						newHall1 = curHall;
						newHall2 = null;
					}

					if (a > curRoom.maxA + 2)		//если неподеленная стена больше чема максимум						
						halls.Enqueue (newHall1);	//то вернуть облачть в очередь
					else {							//иначе наполнить ее предметами и закончить с ней
						b = (int)(y - newHall1.v1.y);
						GenerateFurniture (newHall1.v1, a, b, curRoom.objects);
					}
					if (newHall2 != null) halls.Enqueue (newHall2);

					break;
				case 1:
					delta = (int)(Mathf.Sign (b - curRoom.minB) * Random.Range (0, Mathf.Abs (b - curRoom.minB) + 1));

					w = Random.Range (curRoom.minB - delta, curRoom.maxA - delta);
					if (w < curHall.v1.x + 1 || w > a - 2)		// delta слишком маленькое или слшком большое
						w = Random.Range (curRoom.minB, curRoom.maxA);		
					if (w > a - 2)								// стена ставится за пределами комнаты
						w = a / 2;
					int x = (int)(curHall.v1.x + w);
					// двигаем дверь
					if (mapInt [(int)curHall.v2.y + 1, x] != WALL) {
						mapInt [(int)curHall.v2.y + 1, x] = WALL;
						mapInt [(int)curHall.v2.y + 1, x + 1] = DOOR;
					}
					if (mapInt [(int)curHall.v1.y - 1, x] != WALL) {
						mapInt [(int)curHall.v1.y - 1, x] = WALL;
						mapInt [(int)curHall.v1.y - 1, x - 1] = DOOR;					
					}

					newHall1.v1 = new Vector2 (curHall.v1.x, curHall.v1.y);
					newHall1.v2 = new Vector2 (x - 1, curHall.v2.y);
					newHall2.v1 = new Vector2 (x + 1, curHall.v1.y);
					newHall2.v2 = new Vector2 (curHall.v2.x, curHall.v2.y);

					int doorY = Random.Range ((int)curHall.v1.y, (int)curHall.v2.y);
					for (int j = (int)curHall.v1.y; j <= curHall.v2.y; j++)
						if (j != doorY)
							mapInt [j, x] = WALL;
						else
							mapInt [j, x] = DOOR;
					
					if (b > curRoom.maxA + 2)
						halls.Enqueue (newHall1);
					else {
						a = (int)(x - newHall1.v1.x);
						GenerateFurniture (newHall1.v1, a, b, curRoom.objects);
					}
					if (newHall2 != null) halls.Enqueue (newHall2);

					break;
				}

			}
		}
	}
	void GenerateFurniture(Vector2 v1, int a, int b, List<PairGF> objects)
	{
		int space = ((a - 2)*(b - 2) > 0)? a*b - Random.Range(0, (a - 2)*(b - 2)) : a*b;	//оставшееся пустое место,
		if (space <= 0 || a == 1 || b == 1) return;  //пропускаем 0-dim комнаты и единичные коридоры

		int[,] probMask = new int[a, b];	//маска вероятнистей с преобладанием краев и двойным преобладанием углов, отмечаем 0 у дверей
		float decX = (int)v1.x;
		float decY = (int)v1.y;
		for (int d1 = 1; d1 < a - 1; d1++) {
			probMask [d1, 0] = (mapInt[(int)decY - 1, (int)decX + d1] != DOOR)? 2 : 0;
			probMask [d1, b - 1] = (mapInt[(int)decY + b, (int)decX + d1] != DOOR)? 2 : 0;
			for (int d2 = 1; d2 < b - 1; d2++)
				probMask [d1, d2] = 1;
		}
		for (int d2 = 1; d2 < b - 1; d2++) {
			probMask [0, d2]  = (mapInt[(int)decY + d2, (int)decX - 1] != DOOR)? 2 : 0;
			probMask [a - 1, d2] = (mapInt[(int)decY + d2, (int)decX + a] != DOOR)? 2 : 0;
		}
		probMask [0, 0] = (mapInt[(int)decY,(int) decX - 1] != DOOR && mapInt[(int)decY - 1, (int)decX] != DOOR)? 4 : 0; 
		probMask [0, b - 1] = (mapInt[(int)decY + b - 1, (int)decX - 1] != DOOR && mapInt[(int)decY + b, (int)decX] != DOOR)? 4 : 0;
		probMask [a - 1, 0] = (mapInt[(int)decY, (int)decX + a] != DOOR && mapInt[(int)decY - 1, (int)decX + a - 1] != DOOR)? 4 : 0;
		probMask [a - 1, b - 1] = (mapInt[(int)decY + b, (int)decX + a - 1] != DOOR && mapInt[(int)decY + b - 1, (int)decX + a] != DOOR)? 4 : 0; 

		//Спавн предметов
		float probability = 0;
		float probBonus = 0;
		int spawnAmount = 1;
		bool destroyFlag;
		bool flipX; bool flipY;
		for (int p = 0; p < objects.Count; p++) {
			probability = objects [p].b;
			spawnAmount = 1;
			if (probability > 100) {
				spawnAmount = (int)probability / 100;
				probability -= spawnAmount * 100;
			}
			probability += probBonus;
			for (int pp = 0; pp < spawnAmount; pp++)
			{
				destroyFlag = false;
				flipX = false;
				flipY = false;
				if (Random.Range (0f, 100f) <= probability)
				if (space > 0) { 			
					int probSum = 0;		//находим сумму коэф. по всей маске
					for (int d1 = 0; d1 < a; d1++)
						for (int d2 = 0; d2 < b; d2++)
							probSum += probMask [d1, d2];
					int snailCoord = Random.Range (0, probSum);	//рандомизируем улиточные координаты
					//переводим улиточные коорд. в декартовы коорд. в маске
					decX = 0;
					decY = 0;
					for (int d1 = 0; d1 < a - 1; d1++) {	//кромка
						snailCoord -= probMask [d1, 0];
						if (snailCoord < 0) {
							decX = d1;
							decY = 0;
							break;
						}
					}
					if (snailCoord >= 0)
						for (int d1 = a - 1; d1 > 0; d1--) {
							snailCoord -= probMask [d1, b - 1];
							if (snailCoord < 0) {
								decX = d1; 
								decY = b - 1;
								break;
							}
						}
					if (snailCoord >= 0)
						for (int d2 = 0; d2 < b - 1; d2++) {
							snailCoord -= probMask [a - 1, d2];
							if (snailCoord < 0) {
								decX = a - 1;
								decY = d2;
								break;
							}
						}
					if (snailCoord >= 0)
						for (int d2 = b - 1; d2 > 0; d2--) {
							snailCoord -= probMask [0, d2];
							if (snailCoord < 0) {
								decX = 0;
								decY = d2;
								break;
							}
						}
					if (snailCoord >= 0 && a > 2 && b > 2) {					//внутр. обл.
						decY = 1 + snailCoord / (a - 2);
						decX = 1 + snailCoord % (a - 2);
					}

					//вращение объектов размером 2 (4) или сдвиг объектов размером 1 (4)
					ObjectController objCtrl = objects [p].a.GetComponent<ObjectController> ();
					int objSize = objCtrl.size;
					Vector3 rot = new Vector3 ();

					if (objSize == 1) {
						if (probMask [(int)decX, (int)decY] == 0)
						if (decX > 0 && probMask [(int)decX - 1, (int)decY] != 0)
							decX--;
						else if (decY > 0 && probMask [(int)decX, (int)decY - 1] != 0)
							decY--;
						else if (decX < a - 1 && probMask [(int)decX + 1, (int)decY] != 0)
							decX++;
						else if (decY < b - 1 && probMask [(int)decX, (int)decY + 1] != 0)
							decY++;
						else 
							destroyFlag = true;

						//вращение контейнеров
						if (objCtrl.container) {
							if (decX > 0 && probMask [(int)decX - 1, (int)decY] != 0) {	//дверью к пустому месту
								rot.z = 180;
								probMask [(int)decX - 1, (int)decY] = 0;
							} else if (decY > 0 && probMask [(int)decX, (int)decY - 1] != 0) { 
								rot.z = -90;//?
								probMask [(int)decX, (int)decY - 1] = 0;
							} else if (decY < b - 1 && probMask [(int)decX, (int)decY + 1] != 0) {
								rot.z = 90;
								probMask [(int)decX, (int)decY + 1] = 0;
							} else if (decX < a - 1 && probMask [(int)decX + 1, (int)decY] != 0) {
								rot.z = 0;
								probMask [(int)decX + 1, (int)decY] = 0;
							} else if (decX == 0)		//или задником к стене
								rot.z = 0;
							else if (decY == 0)
								rot.z = 90;
							else if (decY == b - 1)
								rot.z = -90;
							else if (decX == a - 1)
								rot.z = 180;											
						}

						probMask [(int)decX, (int)decY] = 0;
					}
					if (objSize == 2) {
						if (probMask [(int)decX, (int)decY] != 0) {	//вращение вокруг верха
							if (decY > 0 && probMask [(int)decX, (int)decY - 1] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX, (int)decY - 1] = 0;
								decY -= 0.5f;
							} else if (decX > 0 && probMask [(int)decX - 1, (int)decY] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX - 1, (int)decY] = 0;
								decX -= 0.5f;
								rot.z = 90;
							} else if (decX < a - 1 && probMask [(int)decX + 1, (int)decY] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX + 1, (int)decY] = 0;
								decX += 0.5f;
								rot.z = -90;
							} else if (decY < b - 1 && probMask [(int)decX, (int)decY + 1] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX, (int)decY + 1] = 0;
								decY += 0.5f;
							} else
								destroyFlag = true;
						} else if (decY > 0 && probMask [(int)decX, (int)decY - 1] != 0) { //вращение вокруг низа
							decY--;
							if (decY > 0 && probMask [(int)decX, (int)decY - 1] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX, (int)decY - 1] = 0;
								decY -= 0.5f;
								rot.z = 180;
							} else if (decX > 0 && probMask [(int)decX - 1, (int)decY] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX - 1, (int)decY] = 0;
								decX -= 0.5f;
								rot.z = 90;
							} else if (decX < a - 1 && probMask [(int)decX + 1, (int)decY] != 0) {
								probMask [(int)decX, (int)decY] = 0;
								probMask [(int)decX + 1, (int)decY] = 0;
								decX += 0.5f;
								rot.z = -90;
							} else
								destroyFlag = true;
						} else if (decX > 0 && decY > 0 && probMask [(int)decX - 1, (int)decY] != 0 && probMask [(int)decX - 1, (int)decY - 1] != 0) {	//сдвиг влево
							probMask [(int)decX - 1, (int)decY] = 0;
							probMask [(int)decX - 1, (int)decY - 1] = 0;
							decY -= 0.5f;
							decX--;
						} else if (decX < a - 1 && decY > 0 && probMask [(int)decX + 1, (int)decY] != 0 && probMask [(int)decX + 1, (int)decY - 1] != 0) {	//сдвиг вправо
							probMask [(int)decX + 1, (int)decY] = 0;
							probMask [(int)decX + 1, (int)decY - 1] = 0;
							decY -= 0.5f;
							decX++;
						} else
							destroyFlag = true;

						if (decX == a - 1)
							flipX = true;
						if (decY == b - 1)
							flipY = true;

					}
					//вычислить гловальную позицию обьекта
					Vector3 pos = new Vector3(v1.x + decX, v1.y + decY, 0.1f) ;

					//TODO мб собирать контейнеры в очереди

					if (!destroyFlag) {
						var newObj = Instantiate (objects [p].a, pos, Quaternion.Euler(rot));	
						space -= objSize;
						probBonus = 0;
						if (flipX)
							newObj.GetComponent<SpriteRenderer> ().flipX = true;
						if (flipY)
							newObj.GetComponent<SpriteRenderer> ().flipY = true;
					} else {
						probBonus = 10; //?
					}
				}
			}
		}
		
	}
	void GenerateOther()
	{
		int x;
		int y;
		for (int i = 0; i < 100; i++) {
			x = Random.Range (0, width);
			y = Random.Range (0, height);
			if (mapInt [y, x] == EMPTY)
				mapInt [y, x] = TREE;				
		}
	}
	void GenerateEnemies ()
	{
		int x;
		int y;
		for (int i = 0; i < 100; i++) {
			x = Random.Range (0, width);
			y = Random.Range (0, height);
			if (mapInt [y, x] == EMPTY)
				Instantiate (enemies [0], new Vector3 (x, y, 0f), transform.rotation);				
		}
	}
}

#if UNITY_EDITOR 
[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor {

	private MapGenerator trgt;

	int sizeR;
	bool showRooms = true;
	int sizeB;
	bool showBuildings = true;
	int s;
	bool[] showContent; 	//массив для откр. списков
	int ind;
	int[,] index;	//массив для выбр. индексов в попапах

	 
	void OnEnable ()
	{
		trgt = (MapGenerator)target;

		if (trgt.possibleRooms == null)
			trgt.possibleRooms = new List<Room> (); 
	}

	public override void OnInspectorGUI() {
		serializedObject.Update ();
		this.DrawDefaultInspector();
		EditorGUILayout.Space ();

		EditorGUILayout.BeginHorizontal ();
		GUILayout.Button ("Apply");	//TODO ?
		GUILayout.Button ("Revert");
		EditorGUILayout.EndHorizontal ();


		//КОМНАТЫ
		showRooms = EditorGUILayout.Foldout(showRooms, "Rooms");
		//EditorGUILayout.LabelField("Rooms");
		if (showRooms) {
			EditorGUI.indentLevel++;

			sizeR = trgt.possibleRooms.Count; 
			sizeR = EditorGUILayout.IntField ("Size", sizeR);
			//инициализация массива булов для откр.списков
			if (showContent == null)
				showContent = new bool[sizeR]; 
			if (showContent.Length < sizeR) {
				bool[] sC = new bool[sizeR];
				for (int i = 0; i < showContent.Length; i++) {
					sC [i] = showContent [i];
				}
				showContent = sC;
			}

			if (sizeR > trgt.possibleRooms.Count) {
				for (int i = trgt.possibleRooms.Count; i < sizeR; i++) {
					trgt.possibleRooms.Add (new Room ());
				}	
			}

			for (int i = 0; i < sizeR; i++) {
				//№ комнаты и управляющие кнопки
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField (i.ToString ());
				if (GUILayout.Button ("Clear")) {
					trgt.possibleRooms [i] = new Room ();
				}
				if (GUILayout.Button ("X")) {
					trgt.possibleRooms.RemoveAt (i);
					sizeR--;
					continue;
				}	
				EditorGUILayout.EndHorizontal ();

				EditorGUI.indentLevel++;
				Room newRoom = trgt.possibleRooms [i];
				if (newRoom == null)
					newRoom = new Room ();
				//начальная инициализация комнаты
				newRoom.type = EditorGUILayout.TextField ("name", newRoom.type);//!
				EditorGUILayout.BeginHorizontal ();
				newRoom.minA = EditorGUILayout.IntField ("Min", newRoom.minA);
				newRoom.minB = EditorGUILayout.IntField (newRoom.minB);
				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.BeginHorizontal ();
				newRoom.maxA = EditorGUILayout.IntField ("Max", newRoom.maxA);
				newRoom.maxB = EditorGUILayout.IntField (newRoom.maxB);
				EditorGUILayout.EndHorizontal ();

				//массив объектов
				if (newRoom.objects == null)
					newRoom.objects = new List<PairGF> ();

				s = newRoom.objects.Count; 
				s = EditorGUILayout.IntField ("Obj Count", s);

				EditorGUI.indentLevel++;
				showContent [i] = EditorGUILayout.Foldout (showContent [i], "content");
				for (int j = 0; j < s; j++) {
					if (j == newRoom.objects.Count)
						newRoom.objects.Add (new PairGF ());
					EditorGUILayout.BeginHorizontal ();
					PairGF p = new PairGF (); 
					p.a = newRoom.objects [j].a;
					p.b = newRoom.objects [j].b;
					if (showContent [i])
						p.a = (GameObject)EditorGUILayout.ObjectField ((Object)p.a, typeof(GameObject), true);
					if (showContent [i])
						p.b = EditorGUILayout.FloatField (p.b); 
					newRoom.objects [j] = p;
					//удаление эл.
					if (showContent [i])
					if (GUILayout.Button ("X")) {
						newRoom.objects.RemoveAt (j);
						s--;
					}
					EditorGUILayout.EndHorizontal (); 
				}
				EditorGUI.indentLevel--;
			
				trgt.possibleRooms [i] = newRoom;
				//EditorUtility.SetDirty(target);

				EditorGUI.indentLevel--;
			}

			EditorGUI.indentLevel--;
		}

		//подготовка массива имен комнат
		string[] roomOptions = new string[trgt.possibleRooms.Count];
		for (int i = 0; i < trgt.possibleRooms.Count; i++) {
			roomOptions [i] = trgt.possibleRooms [i].type;
		}

		//ЗДАНИЯ
		showBuildings = EditorGUILayout.Foldout(showBuildings, "Buildings");
		//EditorGUILayout.LabelField("Buildings");
		if (showBuildings) {
			EditorGUI.indentLevel++;
			//TODO комнаты через какой нибудь енум
			sizeB = trgt.possibleBuildings.Count; 
			sizeB = EditorGUILayout.IntField ("Size", sizeB);

			if (sizeB > trgt.possibleBuildings.Count) {
				for (int i = trgt.possibleBuildings.Count; i < sizeB; i++) {
					trgt.possibleBuildings.Add (new Building ());
				}	
			}

			index = new int[sizeB, sizeR];
			for (int b = 0; b < sizeB; b++)
				for (int rs = 0; rs < sizeR; rs++)
					for (int r = 0; r < trgt.possibleBuildings [b].rooms.Count; r++)
						if (trgt.possibleBuildings [b].rooms [r].a.type == roomOptions [rs])
							index [b, r] = rs;
					
			if (index.GetLength (0) != sizeB || index.GetLength (1) != sizeR) {
				var temp = new int[sizeB, sizeR];
				int a = index.GetLength (0) >= sizeB ? sizeB : index.GetLength (0);
				int b = index.GetLength (1) >= sizeR ? sizeR : index.GetLength (1);
				for (int i = 0; i < a; i++)
					for (int j = 0; j < b; j++)
						temp [i, j] = index [i, j];
				index = temp;
			}


			for (int i = 0; i < sizeB; i++) {
				//
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField (i.ToString ());
				if (GUILayout.Button ("Clear")) {
					trgt.possibleBuildings [i] = new Building ();
				}
				if (GUILayout.Button ("X")) {
					trgt.possibleBuildings.RemoveAt (i);
					sizeB--;
					continue;
				}	
				EditorGUILayout.EndHorizontal ();

				EditorGUI.indentLevel++;
				Building newBuild = trgt.possibleBuildings [i];
				if (newBuild == null)
					newBuild = new Building ();
				//начальная инициализация комнаты
				newBuild.type = EditorGUILayout.TextField ("name", newBuild.type);//!
				EditorGUILayout.BeginHorizontal ();
				newBuild.minSqr = EditorGUILayout.IntField ("Min Sqr|Dim", newBuild.minSqr);
				newBuild.minDim = EditorGUILayout.IntField (newBuild.minDim);
				EditorGUILayout.EndHorizontal ();

				//массив комнат
				if (newBuild.rooms == null)
					newBuild.rooms = new List<PairRI> ();

				s = newBuild.rooms.Count; 
				s = EditorGUILayout.IntField ("Room Count", s);
				if (s > trgt.possibleRooms.Count) {
					if (newBuild.rooms.Count > trgt.possibleRooms.Count)
						newBuild.rooms.RemoveRange (trgt.possibleRooms.Count, s - trgt.possibleRooms.Count); 
					s = trgt.possibleRooms.Count;
				} 

				EditorGUI.indentLevel++;
				//showContent [i] = EditorGUILayout.Foldout (showContent [i], "content");
				for (int j = 0; j < s; j++) {
					if (j == newBuild.rooms.Count)
						newBuild.rooms.Add (new PairRI ());
					EditorGUILayout.BeginHorizontal ();
					PairRI p = new PairRI (); 
					p.a = newBuild.rooms [j].a;
					p.b = newBuild.rooms [j].b;

					ind = EditorGUILayout.Popup (index [i, j], roomOptions);
					if (ind >= sizeR)
						ind = sizeR - 1;
					index [i, j] = ind; 
					
					p.a = trgt.possibleRooms [index [i, j]];
					//if (showContent [i])
					p.b = EditorGUILayout.IntField (p.b); 
					newBuild.rooms [j] = p;
					//удаление эл.
					//if (showContent [i])
					if (GUILayout.Button ("X")) {
						newBuild.rooms.RemoveAt (j);
						s--;
					}
					EditorGUILayout.EndHorizontal (); 
				}
				EditorGUI.indentLevel--;

				trgt.possibleBuildings [i] = newBuild;

				EditorGUI.indentLevel--;
			}

			EditorGUI.indentLevel--;
		}
		serializedObject.ApplyModifiedProperties ();
	}

}
#endif
	