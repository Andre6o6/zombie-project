﻿using UnityEngine;
using System.Collections;
public class CameraFollow : MonoBehaviour
{
	public GameObject player;
	public float delayTime;
	public float radius;
    float minRadius;

	Vector2 axialVelocity;
	public LineRenderer line;


	// Use this for initialization
	void Start ()
	{
		axialVelocity = new Vector2(0, 0);
		minRadius = 0.05f * radius;
	}
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButton(1))
		{
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			if (Vector2.Distance (transform.position, mousePosition) >= minRadius) {
				transform.position = new Vector3 (Mathf.SmoothDamp (transform.position.x, mousePosition.x, ref axialVelocity.x, delayTime),
					Mathf.SmoothDamp (transform.position.y, mousePosition.y, ref axialVelocity.y, delayTime),
					transform.position.z);

				if (Vector2.Distance (transform.position, player.transform.position) >= radius) {
					float x, y;
					Vector3 dirToCamera = Camera.main.transform.position - player.transform.position;

					if (Mathf.Abs (dirToCamera.x) > 1f) {
						float k, m;

						k = dirToCamera.y / dirToCamera.x;
						m = player.transform.position.y - k * player.transform.position.x;

						float a, b, c;
						a = k * k + 1;
						b = a * player.transform.position.x;
						c = b * player.transform.position.x - radius * radius;
						float d;

						if (Mathf.Abs (a) > 0.1f) {
							d = radius * Mathf.Sqrt (a);
							x = (b + d) / a;
							y = k * x + m;

							if (Vector2.Dot (dirToCamera, (new Vector2 (x, y) - (Vector2)player.transform.position)) < 0) {
								x = (b - d) / a;
								y = k * x + m;
							}
						} else {
							Debug.Log ("bbbbb");
							x = -c / b;
							y = k * x + m;
						}
					} else {
						Debug.Log (dirToCamera.x);
						x = transform.position.x;
						y = player.transform.position.y + radius;
						if (Vector2.Dot (dirToCamera, (new Vector2 (x, y) - (Vector2)player.transform.position)) < 0) {
							y -= radius * 2;
						}
					}

					transform.position = new Vector3 (x, y, transform.position.z);
				}
			}

		}
		else
		{
			transform.position = new Vector3 (Mathf.SmoothDamp (transform.position.x, player.transform.position.x, ref axialVelocity.x, delayTime),
											  Mathf.SmoothDamp (transform.position.y, player.transform.position.y, ref axialVelocity.y, delayTime),
															    transform.position.z);
		}

	}
}