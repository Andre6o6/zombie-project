﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelTransition : MonoBehaviour {
	public delegate void Action();

	public static Action OnCheckpointSave;
	public static Action OnCheckpointLoad;

	public string nextSceneName;
	public int nextSceneId;

	void Awake () {
		//OnCheckpointSave = null;
		//OnCheckpointLoad = null;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			OnCheckpointSave();
			Debug.Log ("Successfully Saved");

			if (nextSceneName != "") 
				SceneManager.LoadScene (nextSceneName);
			else
				SceneManager.LoadScene (nextSceneId);
		}
	}
	public static void LoadCheckpoint()
	{
		OnCheckpointLoad();
	}
}
