﻿//using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class IngameMenuUI : MonoBehaviour {

    private ModalPanel[] menuPanel;

    private UnityAction resumeAction;
    private UnityAction restartAction;
    private UnityAction optionsAction;
    private UnityAction menuAction;

    int ModalPanelIndex = 0;
    int OptionsPanel = 0;
    int ChoicePanel = 0;

    void Awake()
    {
        menuPanel = ModalPanel.Instance();

        AudioListener.pause = false;

        resumeAction = new UnityAction(ResumeFunction);
        restartAction = new UnityAction(RestartFunction);
        optionsAction = new UnityAction(OptionsFunction);
        menuAction = new UnityAction(MenuFunction);


        for (int i = 0; i < 4; ++i)
        {
            if (menuPanel[i].gameObject.name == "SomeShit")
            {
                ModalPanelIndex = i;
            }
			else if (menuPanel[i].gameObject.name == "SomeShitOne")
            {
                OptionsPanel = i;
            }
            else if (menuPanel[i].gameObject.name == "SomeShitTwo")
            {
                ChoicePanel = i;
            }
        }
    }

    public void IngameMenuPanel()
    {
        Time.timeScale = 0F;
        menuPanel[ModalPanelIndex].ChoiceIngameMenu("", ResumeFunction, RestartFunction, OptionsFunction, MenuFunction);
    }

    void ResumeFunction()
    {
		GameManager.instance.player.enabled = true;
        Time.timeScale = 1F;
    }

    void RestartFunction()
    {
        menuPanel[ChoicePanel].Choice("Do you want to restart?", YesRestartFunction, NoFunctionTwo);
        Time.timeScale = 1F;
    }

    void OptionsFunction()
    {
		Debug.Log("HeyGuys" + OptionsPanel);
        menuPanel[OptionsPanel].Choice(SoundFunction, FullScreenFunction, NoFunctionOne);
		Debug.Log("HeyGuys" + OptionsPanel);
    }

    void MenuFunction()
    {
        menuPanel[ChoicePanel].Choice("Exit to main menu?", YesMenuFunction, NoFunctionTwo);
        Time.timeScale = 1F;
    }

    void YesRestartFunction()
    {
        Time.timeScale = 1F;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    void FullScreenFunction()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    void SoundFunction()
    {
        AudioListener.pause = !AudioListener.pause;
    }

    void NoFunctionOne()
    {
        Time.timeScale = 0F;
        menuPanel[ModalPanelIndex].ChoiceIngameMenu("", ResumeFunction, RestartFunction, OptionsFunction, MenuFunction);
    }

    void NoFunctionTwo()
    {

    }

    void YesMenuFunction()
    {
		SceneManager.LoadScene("Menu");
        Time.timeScale = 1F;
    }
}
