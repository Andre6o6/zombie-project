﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DialoguePanels : MonoBehaviour {

    private ModalPanel[] modalPanel;

    int index = 0;

    int ModalPanelIndex = 0, AboutPanelIndex = 0, SettingsPanelIndex = 0, DialoguePanelIndex = 0;

    class Speech
    {
        private string name;
        private string phrase;

        public Speech(string name, string phrase)
        {
            this.name = name;
            this.phrase = phrase;
        }

        public string getName()
        {
            return name;
        }

        public string getPhrase()
        {
            return phrase;
        }
    }

    Speech[] currentSpeech;
    //Speech[] vaultSceneStartTriggerPhrases = { new Speech(" ","..."), new Speech("Hero","Rosa?"), new Speech(" ","..."), new Speech("Hero","Rosa, where are you?") };
	Speech[] vaultSceneStartTriggerPhrases = { new Speech("Hero","Rose, I'm going out. Need to scavenge neighborhood before we run out of food."), new Speech("Rose","OK, sure."), new Speech("Hero","..."), new Speech("Hero","That's all you've got to say?"), new Speech("Rose","Oh, c'mon dad, don't start this again!") };
    Speech[] vaultSceneToiletTriggerPhrases = { new Speech("Hero", "Rose!"), new Speech("Hero", "(whispering)\nShit, shit, shit!") };
    Speech[] vaultSceneKitchenTriggerPhrases = { new Speech("Hero", "Rose, answer me!"), new Speech(" ", "..."), new Speech("Hero", "(whispering)\nOh, God, please, no.") };
	Speech[] vaultSceneExitTriggerPhrases = { new Speech("Rose", "Hey!"), new Speech("Hero", "...?"), new Speech("Rose", "Take care.") };

    void Awake () {

        modalPanel = ModalPanel.Instance();

        for (int i = 0; i < 3; ++i)
        {
            switch (modalPanel[i].gameObject.name)
            {
                case "SomeShit":
                    ModalPanelIndex = i;
                    break;
                case "SomeShitOne":
                    AboutPanelIndex = i;
                    break;
                case "SomeShitTwo":
                    SettingsPanelIndex = i;
                    break;
                case "SomeShitThree":
                    DialoguePanelIndex = i;
                    break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        index = 0;

        if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Vault"))
        {
            switch (this.name)
            {
                case "Start Trigger":
                    if (other.gameObject.tag == "Player")
                    {
                        Dialogue(vaultSceneStartTriggerPhrases);
                    }
                    break;
                case "Toilet Trigger":
                    if (other.gameObject.tag == "Player")
                    {
                        Dialogue(vaultSceneToiletTriggerPhrases);
                    }
                    break;
                case "Kitchen Trigger":
                    if (other.gameObject.tag == "Player")
                    {
                        Dialogue(vaultSceneKitchenTriggerPhrases);
                    }
                    break;
                case "Exit Trigger":
                    if (other.gameObject.tag == "Player")
                    {
                        Dialogue(vaultSceneExitTriggerPhrases);
                    }
                    break;
            }
        }
    }

    void ActivateImage(string name)
    {
        switch (name)
        {
            case "Hero":
                modalPanel[DialoguePanelIndex].iconImage.gameObject.SetActive(true);
                modalPanel[DialoguePanelIndex].iconImage2.gameObject.SetActive(false);
                break;
            case "Rose":
                modalPanel[DialoguePanelIndex].iconImage2.gameObject.SetActive(true);
                modalPanel[DialoguePanelIndex].iconImage.gameObject.SetActive(false);
                break;
            case " ":
                modalPanel[DialoguePanelIndex].iconImage2.gameObject.SetActive(false);
                modalPanel[DialoguePanelIndex].iconImage.gameObject.SetActive(false);
                break;
        }
    }

    void Dialogue(Speech[] speech)
    {
		GameManager.instance.player.enabled = false;
        Time.timeScale = 0F;
        currentSpeech = speech;
        ActivateImage(currentSpeech[0].getName());
        modalPanel[DialoguePanelIndex].JustPanel(currentSpeech[0].getPhrase(), NextFunctionWithTurningOfTheTrigger);
    }

    void NextFunctionWithTurningOfTheTrigger()
    {
        ++index;
        if(index < currentSpeech.Length)
        {
            ActivateImage(currentSpeech[index].getName());
            modalPanel[DialoguePanelIndex].JustPanel(currentSpeech[index].getPhrase(), NextFunctionWithTurningOfTheTrigger);
        }
        else
        {
			GameManager.instance.player.enabled = true;
            Time.timeScale = 1F;
            this.gameObject.SetActive(false);
        }
    }
}
