﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class Pathfinding : MonoBehaviour {

	private static Pathfinding _instance;
	public static Pathfinding instance
	{
		get
		{
			if (_instance == null) Debug.LogError("No Pathfinding instance found");
			return _instance;
		}
	}
    public int height, width;
    public int[][] mapArray;
    public GameObject map;

	int blockedCells;
    float coef;

    void Awake()
    {
		_instance = this;

        SpriteRenderer mapRenderer = map.GetComponent<SpriteRenderer>();

        coef = height / (mapRenderer.sprite.rect.height / mapRenderer.sprite.pixelsPerUnit);
        mapArray = new int[height][];
        width = Mathf.RoundToInt(height * mapRenderer.sprite.rect.width / mapRenderer.sprite.rect.height);

        for (int i = 0; i < height; i++)
        {
            mapArray[i] = new int[width];
        }

        GameObject[] walls = GameObject.FindGameObjectsWithTag("Box");
        BoxCollider2D[] wallColliders = new BoxCollider2D[walls.Length];

        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Polygon");
        PolygonCollider2D[] obstacleColliders = new PolygonCollider2D[obstacles.Length];

        GameObject[] circles = GameObject.FindGameObjectsWithTag("Circle");
        CircleCollider2D[] circleColliders = new CircleCollider2D[circles.Length];

        for (int i = 0; i < walls.Length; i++)
        {
            wallColliders[i] = walls[i].GetComponent<BoxCollider2D>();
        }

        for (int i = 0; i < obstacles.Length; i++)
        {
            obstacleColliders[i] = obstacles[i].GetComponent<PolygonCollider2D>();
        }

        for (int i = 0; i < circles.Length; i++)
        {
            circleColliders[i] = circles[i].GetComponent<CircleCollider2D>();
        }

        Vector2[] points;
        Vector2[] arrayPoints;
        int x0, y0, x1, y1;

		float colliderRadius = 2*/*unit.GetComponent<CircleCollider2D>().radius*/1.44F;
		blockedCells = Mathf.RoundToInt (colliderRadius * coef) - 2;
	
        for (int i = 0; i < obstacles.Length; i++)
        {
            points = obstacleColliders[i].points;
            arrayPoints = new Vector2[points.Length];

            for (int j = 0; j < points.Length; j++)
            {
                arrayPoints[j] = UnitToArray(obstacles[i].transform.position + (Vector3)points[j]);
            }

			Vector2 direction;

            for (int j = 0; j < arrayPoints.Length - 1; j++)
            {
				direction = (Vector2)(ArrayToUnit(arrayPoints [j]) - obstacles [i].transform.position) + obstacleColliders[i].offset;

				x0 = (int)Mathf.Clamp((arrayPoints[j].x + blockedCells * (short)Mathf.Sign(direction.x)), 0, width - 1);
				y0 = (int)Mathf.Clamp((arrayPoints[j].y + blockedCells * (short)Mathf.Sign(direction.y)), 0, width - 1);

				direction = (Vector2)(ArrayToUnit(arrayPoints [j + 1]) - obstacles[i].transform.position) + obstacleColliders[i].offset;

				x1 = (int)Mathf.Clamp((arrayPoints[j + 1].x + blockedCells * (short)Mathf.Sign(direction.x)), 0, width - 1);
				y1 = (int)Mathf.Clamp((arrayPoints[j + 1].y + blockedCells * (short)Mathf.Sign(direction.y)), 0, width - 1);

                LineInit(x0, y0, x1, y1);
            }

			direction = (Vector2)(ArrayToUnit(arrayPoints [arrayPoints.Length - 1]) - obstacles [i].transform.position) + obstacleColliders[i].offset;

			x0 = (int)Mathf.Clamp((arrayPoints[arrayPoints.Length - 1].x + blockedCells * (short)Mathf.Sign(direction.x)), 0, width - 1);
			y0 = (int)Mathf.Clamp((arrayPoints[arrayPoints.Length - 1].y + blockedCells * (short)Mathf.Sign(direction.y)), 0, width - 1);

			direction = (Vector2)(ArrayToUnit(arrayPoints [0]) - obstacles[i].transform.position) + obstacleColliders[i].offset;

			x1 = (int)Mathf.Clamp((arrayPoints[0].x + blockedCells * (short)Mathf.Sign(direction.x)), 0, width - 1);
			y1 = (int)Mathf.Clamp((arrayPoints[0].y + blockedCells * (short)Mathf.Sign(direction.y)), 0, width - 1);

            LineInit(x0, y0, x1, y1);

			Fill(UnitToArray(obstacles[i].transform.position));
        }

		blockedCells += 2;
		Vector2 upperBound, lowerBound;
		Vector3 correction = Vector3.zero;

        for (int i = 0; i < walls.Length; i++)
        {
			correction = /*wallColliders [i].size.x > wallColliders [i].size.y ? new Vector3 (0, colliderRadius/2, 0) :*/ new Vector3 (colliderRadius/2 - 0.5f, colliderRadius/2 - 0.5f, 0);

			upperBound = UnitToArray(walls[i].transform.position + (Vector3)wallColliders[i].offset + (Vector3)wallColliders[i].size/2 + correction);
			lowerBound = UnitToArray(walls[i].transform.position + (Vector3)wallColliders[i].offset - (Vector3)wallColliders[i].size/2 - correction);

            for (int k = (int)lowerBound.x; k <= upperBound.x; k++)
            {
				for (int j = (int)lowerBound.y; j <= upperBound.y; j++)
				{
					mapArray[j][k] = -1;
					mapArray[j][k] = -1;
				}
            }
        }

        int radius;
        Vector2 center;

        for (int i = 0; i < circles.Length; i++)
        {
			radius = Mathf.RoundToInt(circleColliders[i].radius * coef) + blockedCells;
            center = UnitToArray(circles[i].transform.position + (Vector3)circleColliders[i].offset);

            CircleInit(radius, center);
			Fill(UnitToArray(circles[i].transform.position + (Vector3)circleColliders[i].offset));
        }
    }

	private void DisplayMap()
	{
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (mapArray [i] [j] == -1)
					GameObject.CreatePrimitive (PrimitiveType.Cube).transform.position = new Vector3 (j, i, 0); 
			}
		}
	}

    public List<Vector3> GetPath(Vector3 start, Vector3 end)
    {
        //Волновой алгоритм

        Vector2 convStart = UnitToArray(start);
        Vector2 convEnd = UnitToArray(end);

        int currentNumber = 1;

		int[][] tempArray = new int[height][];

		//костыльCounter+=10;

		for (int i = 0; i < height; i++) 
		{
			tempArray [i] = new int[width];

			for (int j = 0; j < width; j++)
			{
				tempArray [i][j] = mapArray [i][j];
			}
		}

		if (tempArray [(int)convEnd.y] [(int)convEnd.x] == -1)  
		{ 
			AdjustPoint(ref convEnd);
			Debug.Log (tempArray [(int)convEnd.y] [(int)convEnd.x]);

		}

		if(tempArray [(int)convStart.y] [(int)convStart.x] == -1 )
		{
			AdjustPoint (ref convStart);
			Debug.Log (tempArray [(int)convStart.y] [(int)convStart.x]);
		}

		tempArray[(int)convStart.y][(int)convStart.x] = 1;

		Queue<Vector2> pointQueue = new Queue<Vector2>();
		pointQueue.Enqueue (new Vector2((int)convStart.x, (int)convStart.y));

		while (tempArray[(int)convEnd.y][(int)convEnd.x] == 0)
        {
//Короче, я облажался и не запуллил перед тем как изменять скрипт,
//вроде как это все что я удалил, но если что-то вдруг перестало работать, можно просто затереть весь файл более ранней версией
//
//			for (int i = 0; i < height; i++)
//			{
//				for (int j = 0; j < width; j++)
//				{
//					if (tempArray[i][j] == currentNumber)
//					{
//						for (int k = -1; k <= 1; k++)
//						{
//							for (int l = -1; l <= 1; l++)
//							{
//								if ((i + k) >= 0 && (i + k) < height && (j + l) >= 0 && (j + l) < width && (k != 0 || l != 0))
//								{
//									if (tempArray[i + k][j + l] == 0)
//									{
//										tempArray[i + k][j + l] = currentNumber + 2 + Mathf.Abs(k * l);
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//
//			currentNumber++;


			Vector2 cur = pointQueue.Dequeue ();
			currentNumber = tempArray [(int)cur.y] [(int)cur.x];

			if (cur.y - 1 >= 0) {
				if (tempArray [(int)cur.y - 1] [(int)cur.x] == 0) {
					tempArray [(int)cur.y - 1] [(int)cur.x] = currentNumber + 1;
					pointQueue.Enqueue (new Vector2 (cur.x, cur.y - 1));

				}
			}
			if (cur.x - 1 >= 0) {
				if (tempArray [(int)cur.y] [(int)cur.x - 1] == 0) {
					tempArray [(int)cur.y] [(int)cur.x - 1] = currentNumber + 1;
					pointQueue.Enqueue (new Vector2 (cur.x - 1, cur.y));

				}
			}
			if (cur.y + 1 < height) {
				if (tempArray [(int)cur.y + 1] [(int)cur.x] == 0) {
					tempArray [(int)cur.y + 1] [(int)cur.x] = currentNumber + 1;
					pointQueue.Enqueue (new Vector2 (cur.x, cur.y + 1));

				}
			}	
			if (cur.x + 1 < width) {
				if (tempArray [(int)cur.y] [(int)cur.x + 1] == 0) {
					tempArray [(int)cur.y] [(int)cur.x + 1] = currentNumber + 1;
					pointQueue.Enqueue (new Vector2 (cur.x + 1, cur.y));

				}
			}
            
        }
		pointQueue.Clear ();

        List<Vector3> path = new List<Vector3>();
        int x = (int)convEnd.x, y = (int)convEnd.y, sx = (int)convStart.x, sy = (int)convStart.y;
		int maxValue = tempArray[y][x];

		//Похоже иногда здесь получается бесконечный цикл
        while (x != sx || y != sy)
        {
			path.Insert(0, ArrayToUnit(convEnd));

            for (int k = -1; k <= 1; k++)
            {
                for (int l = -1; l <= 1; l++)
                {
                    if ((x + k) >= 0 && (x + k) < width && (y + l) >= 0 && (y + l) < height && (k != 0 || l != 0))
                    {
						if (tempArray[y + l][x + k] < maxValue && tempArray[y + l][x + k] > 0)
                        {
							convEnd = new Vector2(x + k, y + l);
							maxValue = tempArray[y + l][x + k];
                        }
                    }
                }
            }

			x = (int)convEnd.x;
			y = (int)convEnd.y;
        }
        return path;
    }

	void AdjustPoint(ref Vector2 point)
	{
		if (mapArray[(int)point.y] [(int)point.x + blockedCells]  == 0) {
			point += new Vector2 (blockedCells, 0);
		} else if (mapArray [(int)point.y] [(int)point.x - blockedCells] == 0) {
			point -= new Vector2 (blockedCells, 0);
		} else if (mapArray [(int)point.y + blockedCells][(int)point.x]  == 0) {
			point += new Vector2 (0, blockedCells);
		} else if (mapArray  [(int)point.y - blockedCells][(int)point.x] == 0) {
			point -= new Vector2 (0, blockedCells);
		}
	}

    void CircleInit(int radius, Vector2 center)
    {
        int x = 0;
        int y = radius;
        int delta = 1 - 2 * radius;
        int error = 0;
        int x1 = (int)center.x;
        int y1 = (int)center.y;

		int py, px, my, mx;

        while (y >= 0)
        {
			py = (int)Mathf.Clamp (y1 + y, 0, height - 1);
			my = (int)Mathf.Clamp (y1 - y, 0, height - 1);
			px = (int)Mathf.Clamp (x1 + x, 0, width - 1);
			mx = (int)Mathf.Clamp (x1 - x, 0, width - 1);

            mapArray[py][px] = -1;
            mapArray[my][px] = -1;
            mapArray[py][mx] = -1;
            mapArray[my][mx] = -1;

            error = 2 * (delta + y) - 1;

            if ((delta < 0) && (error <= 0))
            {
                delta += 2 * ++x + 1;
                continue;
            }

            error = 2 * (delta - x) - 1;

            if ((delta > 0) && (error > 0))
            {
                delta += 1 - 2 * --y;
                continue;
            }

            x++;
            delta += 2 * (x - y);
            y--;
        }
    }
    void LineInit(int x0, int y0, int x1, int y1)
    {
        bool steep = Mathf.Abs(y1 - y0) > Mathf.Abs(x1 - x0); // Проверяем рост отрезка по оси икс и по оси игрек
                                                              // Отражаем линию по диагонали, если угол наклона слишком большой
        if (steep)
        {
            Swap(ref x0, ref y0); // Перетасовка координат вынесена в отдельную функцию для красоты
            Swap(ref x1, ref y1);
        }

        // Если линия растёт не слева направо, то меняем начало и конец отрезка местами
        if (x0 > x1)
        {
            Swap(ref x0, ref x1);
            Swap(ref y0, ref y1);
        }

        int dx = x1 - x0;
        int dy = Mathf.Abs(y1 - y0);
        int error = dx / 2; // Здесь используется оптимизация с умножением на dx, чтобы избавиться от лишних дробей
        int ystep = (y0 < y1) ? 1 : -1; // Выбираем направление роста координаты y
        int y = y0;

        for (int x = x0; x <= x1; x++)
        {
            mapArray[steep ? x : y][steep ? y : x] = -1; // Не забываем вернуть координаты на место

            error -= dy;
            if (error < 0)
            {
                y += ystep;
                error += dx;
            }
        }
    }
    void Swap(ref int a, ref int b)
    {
        a += b;
        b = a - b;
        a -= b;
    }
    bool Fill(Vector2 center)
    {
        if (center.y < 0 || center.y >= height || center.x < 0 || center.x >= width || mapArray[(int)center.y][(int)center.x] == -1)
        {
            return true; 
        }

        mapArray[(int)center.y][(int)center.x] = -1;

        Fill(center + Vector2.up);
        Fill(center + Vector2.down);
        Fill(center + Vector2.left);
        Fill(center + Vector2.right);

        return true;
    }


	Vector2 UnitToArray(Vector3 point)
	{
		int x, y;
		
		x = Mathf.RoundToInt(point.x * coef);
		y = Mathf.RoundToInt(point.y * coef);

		return new Vector2 (Mathf.Clamp(x, 0, width - 1), Mathf.Clamp(y, 0, height - 1));
    }
	Vector3 ArrayToUnit(Vector2 point)
    {
        return new Vector3(point.x / coef, point.y / coef);
    }

	void OnDestroy()
	{
		_instance = null;
	}
}
