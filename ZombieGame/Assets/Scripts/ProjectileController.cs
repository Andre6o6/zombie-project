﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour 
{
    public float overLifetime;
    public float velocity;
    public int damage;
	public Rigidbody2D rigidbody2d;
	public Rigidbody2D parentRigidbody;

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, overLifetime);
		rigidbody2d.velocity = transform.up * velocity;
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag != "Player" && other.tag != "Enemy" && other.gameObject.name !="Lake" && other.gameObject.tag != "Projectile") 
		{
			Destroy (gameObject);
		}
	}
}
