﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : Creature
{
	public int damage;
	public float hitRate;		//amount of time between two enemy hits
	public float hitRange; 
	public float minRange;
	public float sightRange;
	public float sightAngle;

	public float rotationSpeed;
	public GameObject projectile;
	public Collider2D hand;
	public GameObject/*?*/ shepherd = null;	//ведущий

	Animator anim;
    Vector2 directionToPlayer;
	GameObject instProjectile;
	bool canHit;
	bool followingPath;
	float passedTime;
	Vector2 destination;
	bool see;

	Creature[] preys; 	//массив возможных целей - игрок(и), нпс'ы и т.д.
	Creature prey; 		//текущая цель
	bool isTouchngPlayer = false;

	void Start()
	{
		anim = GetComponent<Animator> ();
		rigid = GetComponent<Rigidbody2D> ();
		canHit = true;
		followingPath = false;
		destination = transform.position;

		//StartCoroutine (MoveToPlayer());
		preys = new Creature[]{GameManager.instance.player};
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Projectile")
		{
			health -= other.gameObject.GetComponent<ProjectileController>().damage;
//			if (Vector2.Distance (transform.position, PlayerController.instance.transform.position) < 5f)		//?
//				destination = PlayerController.instance.transform.position;
			Destroy(other.gameObject);
		}
	}

    void OnCollisionEnter2D(Collision2D other)
    {
//		if (other.gameObject.tag == "Projectile") 
//		{
//			health -= other.gameObject.GetComponent<ProjectileController> ().damage;
//			Destroy (other.gameObject);
//		}

		if (other.gameObject.tag == "Enemy") {
			CheckSomething ();	//нужно ли
			if (!see) {
				//float delta = Vector2.Distance (transform.position, PlayerController.instance.transform.position) - Vector2.Distance (other.gameObject.transform.position, PlayerController.instance.transform.position);
				if (GetInstanceID() < other.gameObject.GetInstanceID() && (other.gameObject.GetComponent<EnemyController> ().followingPath || other.gameObject.GetComponent<EnemyController> ().shepherd != null)) {
					shepherd = other.gameObject;
					followingPath = false;
				} else if (!other.gameObject.GetComponent<EnemyController> ().followingPath && other.gameObject.GetComponent<EnemyController> ().shepherd == null){
					shepherd = null;
				}
			}
		} 
    }

	void OnCollisionStay2D(Collision2D other) {
		if (other.gameObject.tag == "Player") {
			isTouchngPlayer = true;
			//prey = other.gameObject.GetComponent<Creature> ();
		}
	}
	void OnCollisionExit2D(Collision2D other) {
		if (other.gameObject.tag == "Player")
			isTouchngPlayer = false;
	}
		
    void Update ()
	{
		if (health <= 0) {
			Die ();
		}

		if (!canHit && passedTime <= hitRate) {
			passedTime += Time.deltaTime;
		} else {
			anim.SetBool ("Hitting", false);
			canHit = true;
		}

		if (shepherd != null) {
			destination = shepherd.transform.position;
		}

		if (!followingPath && !see && Vector2.Distance (transform.position, GameManager.instance.player.transform.position) <= GameManager.instance.player.noise / 2)
		{
			StartCoroutine (MoveToPlayer ());
		}

		//CheckSomething();
		see = SearchForPrey();
		if (see) {
			destination = prey.transform.position;
			followingPath = false; 	//не нравится. Убрать.
			if (Vector3.Distance (transform.position, prey.transform.position) <= hitRange) {
				Hit (prey);
			}
		}

		if (!followingPath) {
			MoveToDest ();
		}
	}

	void CheckSomething()
	{ 
		float distanceToPlayer = Vector3.Distance (transform.position, GameManager.instance.player.transform.position);

		//Если слишком близко
		if (distanceToPlayer <= 1.2f/*радиус коллайдера зомби + радиус коллайдера гг + еще немного*/) {
			see = true;
			destination = GameManager.instance.player.transform.position;
			followingPath = false;

			if (distanceToPlayer <= hitRange) {
				Hit ();
			}
		} else if (distanceToPlayer <= sightRange) {	//Если гг в радиусе обзора...
			//Проверка на сектор обзора
			directionToPlayer = new Vector2 (GameManager.instance.player.transform.position.x - transform.position.x,
				GameManager.instance.player.transform.position.y - transform.position.y).normalized;


			if (Vector2.Angle (directionToPlayer, transform.up) < sightAngle / 2) {
				//Проверка на наличие стен между гг и зомби
				see = true;

				RaycastHit2D[] hitMas = Physics2D.RaycastAll (transform.position, directionToPlayer, distanceToPlayer);		

				Debug.DrawRay (transform.position, directionToPlayer);

				foreach (RaycastHit2D hit in hitMas) {
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") {
						see = false;
						break;
					}
				}
				// я категорически не понимаю, зачем нужна эта херь, ну да ладно
				hitMas = Physics2D.RaycastAll ((transform.position + transform.right * 1.5f), directionToPlayer, distanceToPlayer);		

				Debug.DrawRay ((transform.position + transform.right * 1.5f), directionToPlayer);

				foreach (RaycastHit2D hit in hitMas) {
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") {
						see = false;
						break;
					}
				}

				hitMas = Physics2D.RaycastAll ((transform.position - transform.right * 1.5f), directionToPlayer, distanceToPlayer);		

				Debug.DrawRay ((transform.position - transform.right * 1.5f), directionToPlayer);

				foreach (RaycastHit2D hit in hitMas) {
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") {
						see = false;
						break;
					}
				}


				if (see) {
					destination = GameManager.instance.player.transform.position;
					followingPath = false;

					if (distanceToPlayer <= hitRange) {
						Hit ();
					}
				}
			} else if (see) {
				see = false;
			}

		} else if(see) {
			see = false;
		}

	}
	void MoveToDest()
    {
		float distance = Vector2.Distance (transform.position, destination);

		if (distance < hitRange || distance > sightRange * 2) 
		{
			rigid.velocity = Vector2.zero;
			anim.SetFloat ("Velocity", 0f);
		}
		else
		{ 
			Vector3 direction = new Vector3 (destination.x - transform.position.x, destination.y - transform.position.y, 0f).normalized;

			float sign = (Vector3.Angle (direction, Vector3.right) > 90f) ? 1.0f : -1.0f;
			//transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z);
			transform.rotation = Quaternion.Slerp (	transform.rotation, 
													transform.rotation*Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z),
													Time.deltaTime * rotationSpeed );
			
			rigid.velocity = transform.up * velocity;
			anim.SetFloat ("Velocity", velocity);
		} 

	}

	IEnumerator MoveToPlayer()
	{
		followingPath = true;

		List<Vector3> path = Pathfinding.instance.GetPath (transform.position, GameManager.instance.player.transform.position);

		for (int i = 1; i < path.Count; i++) 
		{
			Vector3 direction = new Vector3 (path[i].x - transform.position.x, path[i].y - transform.position.y, 0f).normalized;

			float sign = (Vector3.Angle (direction, Vector3.right) > 90f) ? 1.0f : -1.0f;
			//transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z);
			//Возможно вызовет баги в следовании по пути
			transform.rotation = Quaternion.Slerp (	transform.rotation, 
													transform.rotation*Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z),
													Time.deltaTime * rotationSpeed * 3);
			
			rigid.velocity = transform.up * velocity;
			anim.SetFloat ("Velocity", velocity);

			if (!followingPath) 
			{
				yield break;
			}

			yield return new WaitUntil (() => Vector3.Distance (transform.position, path [i]) <= hitRange);
		}

		destination = transform.position;
		followingPath = false;
		rigid.velocity = Vector2.zero;
		anim.SetFloat ("Velocity", 0f);
	}

	//Небольшое переписывание
	//TODO для удара проверять угол и переписать МувТуДест так, чтоб он мог просто поворачиваться
	bool SearchForPrey() {
		float distanceToPrey;
		Vector2 directionToPrey;
		bool inSight;
		for (int i = 0; i < preys.Length; i++) {
			//проверка расстояния
			distanceToPrey = Vector3.Distance (transform.position, preys [i].transform.position); 
			if (distanceToPrey <= minRange) {
				prey = preys [i];
				return true;
			}
			if (distanceToPrey <= sightRange) {
				//проверка угла обзора
				directionToPrey = new Vector2 (preys [i].transform.position.x - transform.position.x,
					preys [i].transform.position.y - transform.position.y).normalized;
				if (Vector2.Angle (directionToPrey, transform.up) < sightAngle / 2) {
					//проверка на препядствия
					inSight = true;
					RaycastHit2D[] hitMas = Physics2D.RaycastAll (transform.position, directionToPrey, distanceToPrey);		
					Debug.DrawRay (transform.position, directionToPlayer);
					foreach (RaycastHit2D hit in hitMas) {
						if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") {
							inSight = false;
							break;
						}
					}
					if (inSight) {
						prey = preys [i];
						return true;
					}
				}
			}
		}

		//prey = null;	//если не нужно хранить последнюю цель
		return false;
	}

	void Hit ()			//переписал удар, но немного коряво. По хорошему нужен один bool - возможность ударить, и другой bool - возможность нанести урон. Но пока всё работает
	{		//TODO через target
		anim.SetBool ("Hitting", true);

		if (canHit && hand.IsTouching (GameManager.instance.player.GetComponent<CircleCollider2D> ())) 
		{
			GameManager.instance.player.health -= damage;

			passedTime = 0f;
			canHit = false;
		}
	}
	void Hit (Creature target) {
		anim.SetBool ("Hitting", true);

		if (canHit && hand.IsTouching (target.body)) 
		{
			target.GetDamage (damage);
			passedTime = 0f;
			canHit = false;
		}
	}

	void Die ()
    {
		anim.SetBool ("Dead", true);
		transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f);
		Destroy (gameObject.GetComponent<Collider2D>());

		rigid.constraints = RigidbodyConstraints2D.FreezePosition;
		this.enabled = false;
	}
		
}
