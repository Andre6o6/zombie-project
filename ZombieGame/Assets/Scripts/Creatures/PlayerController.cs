﻿using UnityEngine;
using System.Collections;
//using UnityEditor;
using UnityEngine.SceneManagement;

public class PlayerController : Creature
{
    public GameObject playerImage;
    public GameObject head;
    public Animator walkingAnim;
	public Animator mainAnim;
    public WeaponController weapon;
	public WeaponController primaryWpn;
	public WeaponController secondaryWpn;

	public IngameMenuUI IngameMenu;

	public float noise;

    public int maxHealth;
	public int stamina;
	float dmgMlt = 1f;
		
//Мб структурой?

	public int medkits;
	int ammo;	//?
	public int food;
	int fuel;
	int junk;	//?

    Camera mainCamera;
    Vector3 mouseDirection;
	Vector2 velocityDirection;
    float currentVelocity;
    float maxVelocity;
    float sign;
	Collider2D[] cols;	//массив, используемый для кэширования коллайдеров у подбираемого оружия

	void Awake ()
    {
		GameManager.instance.player = this; //мб наоборот

        mainCamera = Camera.main;
        maxVelocity = 3 * velocity;
		//health = maxHealth;
		rigid = GetComponent<Rigidbody2D> ();
		weapon = primaryWpn;
		secondaryWpn.gameObject.SetActive (false);
    }

	void Start()
	{
		//TODO : что-то сделать с этим
		mainAnim.SetFloat ("Hands", 2f);
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.tag == "PickableWpn") 
		{
			if (Input.GetKeyDown(KeyCode.E)) 
			{
				PickWpn (other.gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "enemyProjectile")
		{
			health -= other.gameObject.GetComponent<ProjectileController>().damage;
			Destroy(other.gameObject);
		}
	}

    void Update()
    {
		//иф какой-нибудь-айди-манагера==айди этого
		if (health > 0)
		{
			//Поворот за курсором
			mouseDirection = Input.mousePosition - mainCamera.WorldToScreenPoint (transform.position);
			sign = (Vector3.Angle (mouseDirection, Vector3.right) > 90f) ? 1.0f : -1.0f;
			transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (mouseDirection, Vector3.up) * sign - transform.rotation.eulerAngles.z);

			//Передвижение
			float h = Input.GetAxisRaw ("Horizontal");
			float v = Input.GetAxisRaw ("Vertical");
			Move (h, v);

			//Меню
			//TODO : перенести в GameManager
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				enabled = false;

				IngameMenu.IngameMenuPanel();
			}

			//Стрельба
			if ((Input.GetMouseButton (0) && weapon.automatic) || Input.GetMouseButtonDown (0))
			{
				if (!weapon.melee) {
					if (weapon.Fire ()) mainAnim.SetTrigger ("Shooting");
				}
				else 
					if (weapon.Hit(dmgMlt)) mainAnim.SetTrigger ("Hitting");
			}
			if (Input.GetKeyDown (KeyCode.R))
			{
				StartCoroutine (weapon.Reload());
			}
			//ББ
			if (Input.GetKey (KeyCode.V)) {
				dmgMlt += Time.deltaTime;
			}
			if (Input.GetKeyUp (KeyCode.V)) {
				if (weapon.Hit(dmgMlt)) mainAnim.SetTrigger ("Hitting");
				dmgMlt = 1f;
			}

			//Смена оружия
			if (Input.GetKeyDown (KeyCode.Alpha1) && !weapon.reloading)
			{
				primaryWpn.gameObject.SetActive (true);
				weapon = primaryWpn;
				secondaryWpn.gameObject.SetActive (false);
				mainAnim.SetBool ("twoHanded", true);
				mainAnim.SetFloat ("Hands", 2f);
			}
			if (Input.GetKeyDown (KeyCode.Alpha2) && !weapon.reloading)
			{
				secondaryWpn.gameObject.SetActive (true);
				weapon = secondaryWpn;
				primaryWpn.gameObject.SetActive (false);
				mainAnim.SetBool ("twoHanded", false);
				mainAnim.SetFloat ("Hands", 1f);
			}

			//Звук
			if (noise > 1) noise = noise / Mathf.Pow(2, Time.deltaTime);  	
		}
		else 
		{
			Die ();
		}
    }

	void Move (float h, float v)
	{
		velocityDirection = new Vector2 (h, v).normalized ;

		currentVelocity = velocity;		
		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			currentVelocity = 0.5f * velocity;
		}

		if (rigid.angularVelocity != 0f) 
		{
			rigid.angularVelocity = 0f;
		}

		rigid.velocity = velocityDirection * currentVelocity;
		if (velocityDirection != Vector2.zero)
		{
			mainAnim.SetFloat ("Velosity", currentVelocity);
			walkingAnim.SetFloat ("Velosity", currentVelocity);

			float curNoiseLvl = currentVelocity;	//хорошо бы придумать чего поумнее
			if (noise < curNoiseLvl)
				noise = curNoiseLvl;
		}
		else
		{
			mainAnim.SetFloat ("Velosity", 0f);
			walkingAnim.SetFloat ("Velosity", 0f);

			if (rigid.velocity != Vector2.zero) 
			{
				rigid.velocity = Vector2.zero;
			}
		}
	}

	GameObject PickWpn(GameObject other)
	{
		if (weapon.reloading)
			return null;					
		GameObject thrownWpn;
		other.tag = "Wpn";

		cols = other.GetComponents<Collider2D>();
		for (int i = 0; i < 2; i++) {
			if (cols [i].isTrigger == true)
				cols [i].enabled = false;
			else
				cols [i].enabled = true;
		}

		other.GetComponent<SpriteRenderer> ().sprite = other.GetComponent<WeaponController> ().inhandSprite;

		other.transform.SetParent (primaryWpn.transform.parent);
		WeaponController newWpn = other.GetComponent<WeaponController> ();

		if (newWpn.twoHanded) 
		{
			primaryWpn.gameObject.SetActive (true);
			primaryWpn.transform.SetParent (null);
			primaryWpn.transform.position = new Vector3 (transform.position.x, transform.position.y, 0.02f);
			primaryWpn.gameObject.tag = "PickableWpn";
			primaryWpn.GetComponent<SpriteRenderer> ().sprite = primaryWpn.GetComponent<WeaponController> ().worldSprite;
			thrownWpn = primaryWpn.gameObject;

			cols = primaryWpn.GetComponents<Collider2D>();
			for (int i = 0; i < 2; i++) {
				if (cols [i].isTrigger == true)
					cols [i].enabled = true;
				else
					cols [i].enabled = false;
			}
				
			primaryWpn = newWpn;
			weapon = primaryWpn;
			secondaryWpn.gameObject.SetActive (false);
			//other.transform.localPosition = new Vector3 (0.62f, 0.81f, -0.05f);
			other.transform.localPosition = new Vector3 (0f, 0f, -0.02f);
			other.transform.localRotation = new Quaternion ();
			mainAnim.SetBool ("twoHanded", true);
			mainAnim.SetFloat ("Hands", 2f);

		} 
		else
		{
			secondaryWpn.gameObject.SetActive (true);
			secondaryWpn.transform.SetParent (null);
			secondaryWpn.transform.position = new Vector3 (transform.position.x, transform.position.y, 0.02f);
			secondaryWpn.gameObject.tag = "PickableWpn";
			secondaryWpn.GetComponent<SpriteRenderer> ().sprite = secondaryWpn.GetComponent<WeaponController> ().worldSprite;
			thrownWpn = secondaryWpn.gameObject;

			cols = secondaryWpn.GetComponents<Collider2D>();
			for (int i = 0; i < 2; i++) {
				if (cols [i].isTrigger == true)
					cols [i].enabled = true;
				else
					cols [i].enabled = false;
			}

			secondaryWpn = newWpn;
			weapon = secondaryWpn;
			primaryWpn.gameObject.SetActive (false);
			//other.transform.localPosition = new Vector3 (0.62f, 1.48f, -0.05f);
			other.transform.localPosition = new Vector3 (0f, 0f, -0.02f);
			other.transform.localRotation = new Quaternion ();
			mainAnim.SetBool ("twoHanded", false);
			mainAnim.SetFloat ("Hands", 1f);
		}

		if (!weapon.melee) 
		{
			weapon.RecalculateProjectileShift ();
		}

		return thrownWpn;
	}

	void OnDestroy()
	{
		GameManager.instance.player = null;
	}

	void Die()
	{
		mainAnim.SetTrigger ("Die");

		Destroy (gameObject.GetComponent<Collider2D>());
		//if (mainAnim.GetCurrentAnimatorStateInfo (0).IsName ("Dead")) {
			//mainAnim.Stop ();

			//SceneManager.LoadScene ("Menu");
		//}
	}

	//Сохранение и загрузка
	//TODO : fix shit
//	void OnSave()		
//	{
//		PlayerPrefs.SetFloat ("playerPosX", transform.position.x);
//		PlayerPrefs.SetFloat ("playerPosY", transform.position.y);
//		PlayerPrefs.SetInt ("playerHealth", health);
//		PlayerPrefs.SetString ("playerScene", SceneManager.GetActiveScene().name);	// будем ли сохраняться посреди сцены?
//
//		string wpn1Name = primaryWpn.name;
//		if (wpn1Name.IndexOf('(') != -1) wpn1Name = wpn1Name.Remove(wpn1Name.IndexOf('('));
//		//Debug.Log (wpn1Name + " | " + wpn1Name.IndexOf('(') + " | ");
//		string wpn2Name = secondaryWpn.name;
//		if (wpn2Name.IndexOf('(') != -1) wpn2Name = wpn2Name.Remove(wpn2Name.IndexOf('('));
//		PlayerPrefs.SetString ("player1Wpn", wpn1Name); 
//		PlayerPrefs.SetString ("player2Wpn", wpn2Name);
//	}
//
//	void OnLoad()
//	{
//		if (!PlayerPrefs.HasKey ("playerPosX")) {
//			return;
//		}
//		
//		Vector3 pos = Vector3.zero;
//		if (PlayerPrefs.GetString ("playerScene") == SceneManager.GetActiveScene ().name) {
//			//pos.x = PlayerPrefs.GetFloat ("playerPosX");
//			//pos.y = PlayerPrefs.GetFloat ("playerPosY");
//			//transform.position = pos;
//		} else {
//			PlayerPrefs.SetString ("playerScene", SceneManager.GetActiveScene().name);
//			//???найти все объекты переходов
//			//выбрать тот, который исходит из сцены из PlayerPrefs
//			//загрузиться в его координатах
//		}
//
//		GameManager.instance.player.health = PlayerPrefs.GetInt ("playerHealth");
//
//		Debug.Log ("Inst. Wpn/" + PlayerPrefs.GetString("player1Wpn"));
//		Debug.Log ("Inst. Wpn/" + PlayerPrefs.GetString("player2Wpn"));
//
//		GameObject wpn1 = (GameObject)Instantiate ((GameObject)Resources.Load ("Wpn/" + PlayerPrefs.GetString("player1Wpn")), GameManager.instance.player.transform.position, GameManager.instance.player.transform.rotation);
//		GameObject wpn2 = (GameObject)Instantiate ((GameObject)Resources.Load ("Wpn/" + PlayerPrefs.GetString("player2Wpn")), GameManager.instance.player.transform.position, GameManager.instance.player.transform.rotation);
//		Destroy (GameManager.instance.player.PickWpn (wpn2));
//		Destroy (GameManager.instance.player.PickWpn (wpn1));
//
//		Debug.Log ("loaded on " + SceneManager.GetActiveScene ().name);
//	}

}
