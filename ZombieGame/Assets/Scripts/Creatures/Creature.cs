﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour {

	public float velocity;
	public int health;

	public Collider2D body;
	protected Rigidbody2D rigid;

	public void GetDamage(int damage) {
		health -= damage;
		if (health < 0)
			health = 0;
	}
}
