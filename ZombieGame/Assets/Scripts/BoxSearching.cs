﻿using UnityEngine;
using System.Collections;

public class BoxSearching : MonoBehaviour {
	
	public int medCount;		//Не то что бы очень нужно если у нас будет префаб аптечки и еды
	public int foodCount;
	public GameObject[] items; 	//Массив предметов, лежащих в ящике

	public int medPercent;		//Вероятность выпадения дополнительного ...
	public int foodPercent;
	public int itemPercent;		//Насчет предметов пока хз
	public GameObject[] randomItems;

	public bool empty;

	void OnCollisionEnter2D (Collision2D other){
		if (other.gameObject.tag == "Player" && !empty) {
			if (Random.Range (0, 100) <= medPercent)
				medCount++;
			if (Random.Range (0, 100) <= foodPercent)
				foodCount++;
			
			GameManager.instance.player.medkits += medCount;
			GameManager.instance.player.food += foodCount;
			for (int i = 0; i < items.Length; i++) {
				Instantiate (items [i], transform.position, transform.rotation);
			}

			if (Random.Range (0, 100) <= itemPercent) {
				//здесь должно быть распределение процентов по различным объектам: партоны, оружие
				//или нет))00)
				int i = Random.Range(0, randomItems.Length - 1);
				Instantiate (randomItems [i], transform.position, transform.rotation);
			}

			empty = true;
		}
	}

}
