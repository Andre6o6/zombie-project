﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = (GameManager) FindObjectOfType(typeof(GameManager));

				if ( FindObjectsOfType(typeof(GameManager)).Length > 1 )
				{
					Debug.LogError("[GameManager] More than 1 singelton on scene");
					return _instance;
				}

				if (_instance == null)
				{
					GameObject singleton = new GameObject();
					_instance = singleton.AddComponent<GameManager>();
					singleton.name = "gameManager";

					//DontDestroyOnLoad(singleton);

					Debug.Log("[GameManager] An instance of " + singleton + "' was created without DontDestroyOnLoad.");
				} 
			}
			return _instance;
		}
	}

	public PlayerController player; 
	//public PlayerController[] players;

}
