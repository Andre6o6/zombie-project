﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ZombieSpawn : NetworkBehaviour {

	public GameObject zombie;
	public float period;

	float passedTime;

	// Use this for initialization
	void Start () {
		passedTime = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isServer) {
			passedTime += Time.deltaTime;

			if (passedTime >= period) {
				NetworkServer.SpawnObjects ();
				NetworkServer.Spawn ((GameObject)Instantiate(zombie, transform.position, transform.rotation));
				passedTime = 0;
			}
		}
	}
}
