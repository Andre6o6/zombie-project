﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Linq;

public class EnemyController : NetworkBehaviour
{
	public float health;
	public int damage;
	public float hitRate;		//amount of time between two enemy hits
	public float hitRange;
	public float sightRange;
	public float sightAngle;
	public float veloсity;
	public float rotationSpeed;
	public GameObject projectile;
	public Collider2D hand;
	public GameObject/*?*/ shepherd = null;	//ведущий
	public GameObject corpse;
	
    public PlayerController tPlayer;
    public List<GameObject> possibleTargets;
    public bool b;

	float switchTime, curTime;
	Animator anim;
	Vector2 directionToPlayer;
	Rigidbody2D rigidbody2d;
	GameObject instProjectile;
	bool canHit, followingPath;
	float passedTime;
	Vector2 destination;
	bool see;
	PlayerController[] plist;

	void Start()
	{
		anim = GetComponent<Animator> ();
		rigidbody2d = GetComponent<Rigidbody2D> ();
		canHit = true;
		followingPath = false;
		destination = transform.position;
		possibleTargets = new List<GameObject> ();

        PlayerController[] pc = FindObjectsOfType<PlayerController>();

        if(pc.Length > 0)
        {
		    tPlayer = pc.OrderBy (p => Vector2.SqrMagnitude (p.transform.position -  transform.position)).First();
            StartCoroutine (MoveToPlayer());
        }
		switchTime = 10f;
		curTime = 0f;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
        if (other.gameObject.tag == "Projectile")
        {
                health -= other.gameObject.GetComponent<ProjectileController>().damage;
               //if (Vector2.Distance (transform.position, tPlayer.transform.position) < 5f)		//?//	
              destination = tPlayer.transform.position;
               Destroy(other.gameObject);
        }
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (possibleTargets.Contains (other.gameObject))
		{
			possibleTargets.Remove (other.gameObject);
		}
		if (other.gameObject == tPlayer.gameObject)
		{
			tPlayer = null;see = false;
		}
		if (possibleTargets.Count == 0)
		{
			plist = FindObjectsOfType<PlayerController>();
			tPlayer = plist.ElementAt(Random.Range(0, plist.Length - 1));
		}
		else
		{
			tPlayer = possibleTargets.OrderBy(p => Vector2.SqrMagnitude(p.transform.position - transform.position)).First().GetComponent<PlayerController>();
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		//if (other.gameObject.tag == "Projectile")
		//{
		//		health -= other.gameObject.GetComponent<ProjectileController> ().damage;
		//		Destroy (other.gameObject);
		//}
		if (other.gameObject.tag == "Enemy") 
		{
			CheckSomething ();	//нужно ли
			if (!see)
			{
				//float delta = Vector2.Distance (transform.position, tPlayer.transform.position) - Vector2.Distance (other.gameObject.transform.position, tPlayer.transform.position);
				if (GetInstanceID() < other.gameObject.GetInstanceID() && (other.gameObject.GetComponent<EnemyController> ().followingPath || other.gameObject.GetComponent<EnemyController> ().shepherd != null))
				{
					shepherd = other.gameObject;followingPath = false;
				} 
				else if (!other.gameObject.GetComponent<EnemyController> ().followingPath && other.gameObject.GetComponent<EnemyController> ().shepherd == null)
				{
					shepherd = null;
				}
			}
		}
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (canHit)
			{
				Hit ();
			}
		}
	}

	void Update ()
	{
		curTime += Time.deltaTime;

		if (switchTime >= curTime)
		{
			curTime = 0;

			plist = FindObjectsOfType<PlayerController>();
			tPlayer = plist.ElementAt(Random.Range(0, plist.Length - 1));
		}

		if (health <= 0) 
		{
			Die ();
		}

		if (!canHit && passedTime <= hitRate)
		{
			passedTime += Time.deltaTime;
		} 
		else 
		{
			anim.SetBool ("Hitting", false);
			canHit = true;
		}

		if (shepherd != null) 
		{
			destination = shepherd.transform.position;
		}

		if (tPlayer != null) 
		{
			if (!followingPath && !see && Vector2.Distance (transform.position, tPlayer.transform.position) <= tPlayer.noise / 2) 
			{
				StartCoroutine (MoveToPlayer ());
				Debug.Log ("Зомби услышал. Unity завис.");
			}

			//Если гг в радиусе обзора...
			CheckSomething ();
			if (!followingPath) 
			{
				MoveToDest ();
			}
		}
	}

	void CheckSomething()
	{
		float distanceToPlayer = Vector3.Distance (transform.position, tPlayer.transform.position);
		//Если слишком близко
		if (distanceToPlayer <= 4.2f/*радиус коллайдера зомби + радиус коллайдера гг + еще немного*/) 
		{
			see = true;
			destination = tPlayer.transform.position;
			followingPath = false;
		
			if (distanceToPlayer <= hitRange) 
			{
				Hit ();
			}
		} 
		else if (distanceToPlayer <= sightRange) 
		{	
			//Если гг в радиусе обзора...
			//Проверка на сектор обзора

			directionToPlayer = new Vector2 (tPlayer.transform.position.x - transform.position.x,
											 tPlayer.transform.position.y - transform.position.y).normalized;

			if (Vector2.Angle (directionToPlayer, transform.up) < sightAngle / 2) 
			{
				//Проверка на наличие стен между гг и зомби

				see = true;
				RaycastHit2D[] hitMas = Physics2D.RaycastAll (transform.position, directionToPlayer, distanceToPlayer);

				foreach (RaycastHit2D hit in hitMas) 
				{
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") 
					{see = false;
						break;
					}
				}

				// я категорически не понимаю, зачем нужна эта херь, ну да ладно
				hitMas = Physics2D.RaycastAll ((transform.position + transform.right * 1.5f), directionToPlayer, distanceToPlayer);

				foreach (RaycastHit2D hit in hitMas) 
				{
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle")
					{
						see = false;
						break;
					}
				}

				hitMas = Physics2D.RaycastAll ((transform.position - transform.right * 1.5f), directionToPlayer, distanceToPlayer);

				foreach (RaycastHit2D hit in hitMas)
				{
					if (hit.collider.gameObject.tag == "Box" || hit.collider.gameObject.tag == "Polygon" || hit.collider.gameObject.tag == "Circle") 
					{
						see = false;
						break;
					}
				}
			
				if (see)
				{
					destination = tPlayer.transform.position;followingPath = false;
					if (distanceToPlayer <= hitRange) 
					{
						Hit ();
					}
				}
			}
			else if (see)
			{
				see = false;
			}
		}
		else if(see) 
		{
			see = false;
		}
	}

	void MoveToDest()
	{
		float distance = Vector2.Distance (transform.position, destination);

		if (distance < hitRange || distance > sightRange * 2)
		{
			rigidbody2d.velocity = Vector2.zero;
			anim.SetFloat ("Velocity", 0f);
		}
		else
		{
			Vector3 direction = new Vector3 (destination.x - transform.position.x, destination.y - transform.position.y, 0f).normalized;
			float sign = (Vector3.Angle (direction, Vector3.right) > 90f) ? 1.0f : -1.0f;
			//transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z);
			transform.rotation = Quaternion.Slerp (	transform.rotation,transform.rotation*Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z),Time.deltaTime * rotationSpeed );
			rigidbody2d.velocity = transform.up * veloсity;anim.SetFloat ("Velocity", veloсity);
		}
	}

	IEnumerator MoveToPlayer()
	{
		followingPath = true;
		List<Vector3> path = Pathfinding.instance.GetPath (transform.position, tPlayer.transform.position);

		for (int i = 1; i < path.Count; i++)
		{
			Vector3 direction = new Vector3 (path[i].x - transform.position.x, path[i].y - transform.position.y, 0f).normalized;
			float sign = (Vector3.Angle (direction, Vector3.right) > 90f) ? 1.0f : -1.0f;
			//transform.rotation *= Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z);
			//Возможно вызовет баги в следовании по пути

			transform.rotation = Quaternion.Slerp (	transform.rotation,transform.rotation*Quaternion.Euler (0f, 0f, Vector3.Angle (direction, Vector3.up) * sign - transform.rotation.eulerAngles.z),Time.deltaTime * rotationSpeed * 3);
			rigidbody2d.velocity = transform.up * veloсity;anim.SetFloat ("Velocity", veloсity);

			if (!followingPath)
			{
				yield break;
			}

			yield return new WaitUntil (() => Vector3.Distance (transform.position, path [i]) <= hitRange);
		}

		destination = transform.position;
		followingPath = false;
		rigidbody2d.velocity = Vector2.zero;
		anim.SetFloat ("Velocity", 0f);
	}

	void FixedUpdate()
	{
		
	}

	void Hit ()		
	//переписал удар, но немного коряво. По хорошему нужен один bool - возможность ударить, и другой bool - возможность нанести урон. Но пока всё работает
	{
		anim.SetBool ("Hitting", true);
		if (canHit && hand.IsTouching (tPlayer.GetComponent<CircleCollider2D> ()))
		{
			tPlayer.health -= damage;passedTime = 0f;
			canHit = false;
		}
	}

	[Command]
	void CmdSpawn(GameObject g)
	{
		NetworkServer.Spawn (g);
	}
	void Die ()
	{
		anim.SetBool ("Dead", true);
		transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f);
		NetworkServer.SpawnObjects ();
		GameObject g = (GameObject)Instantiate (corpse, gameObject.transform.position, gameObject.transform.rotation);

		if (isServer)
		{
			NetworkServer.Spawn (g);
		} 
		else {
			CmdSpawn (g);
		}

		NetworkServer.Destroy (gameObject);
	}
}