﻿using UnityEngine;
using System.Collections;

public class ZombieSight : MonoBehaviour {

    EnemyController zombie;

	// Use this for initialization
	void Start () {
        zombie = transform.parent.GetComponent<EnemyController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (zombie.tPlayer != null)
            {
                if (Vector2.SqrMagnitude(transform.position - other.transform.position) > Vector2.SqrMagnitude(transform.position - zombie.tPlayer.transform.position))
                {
                    zombie.tPlayer = other.gameObject.GetComponent<PlayerController>();
                }
                else
                {
                    zombie.possibleTargets.Add(other.gameObject);
                }
            }
            else
            {
                zombie.tPlayer = other.gameObject.GetComponent<PlayerController>();
            }
        }
    }

}

